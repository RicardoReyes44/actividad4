'''
Created on 24 sep. 2020

@author: RSSpe
'''

from Sistema import Sistema


def main():
    sistema = Sistema()
    sistema.ejecutar()


if __name__ == '__main__':
    main()
