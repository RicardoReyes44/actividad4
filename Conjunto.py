'''
Created on 17 ene. 2020

@author: RSSpe
'''

from Config import generarConjuntoDivisores, generarConjuntoPrimo


class Conjunto:
    "" "Clase para administrar las operaciones de conjuntos" ""
    def __init__(self):
        self.__cd = generarConjuntoDivisores()
        self.__cp = generarConjuntoPrimo()


    def union(self) -> set:
        "" "Metodo para calcular la union de conjuntos" ""
        return self.__cd.union(self.__cp)


    def interseccion(self) -> set:
        "" "Metodo para calcular la interseccion de conjuntos" ""
        return self.__cd.intersection(self.__cp)


    def diferencia(self) -> set:
        "" "Metodo para calcular la diferencia de conjuntos" ""
        return self.__cd.difference(self.__cp), self.__cp.difference(self.__cd)
