'''
Created on 24 sep. 2020

@author: RSSpe
'''

from Interfaz import *
from Config import ALTO, ANCHO, TITULO, TEXTO


class Sistema:
    "" "Clase para gestionar el programa" ""
    def __init__(self):
        self.__interfaz = Interfaz(ALTO, ANCHO, TITULO, TEXTO)


    def ejecutar(self) -> None:
        "" "Metodo para iniciar el programa" ""
        self.__interfaz.construir()
