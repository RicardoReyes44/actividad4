'''
Created on 24 sep. 2020

@author: RSSpe
'''

from tkinter import *
from Conjunto import Conjunto


class Interfaz:
    "" "Clase para gestionar la interfaz" ""
    def __init__(self, alto: int, ancho: int, titulo: str, texto: str):
        self.__conjunto = Conjunto()
        self.__raiz = Tk()
        self.__indice = 0
        self.__alto = alto
        self.__ancho = ancho
        self.__raiz.title(titulo)
        self.__raiz.resizable(False, False)

        self.__miframe = Frame(self.__raiz)
        self.__miframe.pack()
        self.__miframe.config(bg="gray")
        
        self.__lsb_texto = Listbox(self.__miframe, width=self.__ancho, height=self.__alto)
        self.__lsb_texto.grid(row=1, column=1, padx=15, pady=15, rowspan=3)
        self.__lsb_texto.config(justify="center")
        
        self.__lbl_info = Label(self.__miframe, text=texto, fg="white", bg="black")
        self.__lbl_info.grid(row=0, column=1, padx=15, pady=15)
        
        self.__btn_union = Button(self.__miframe, width=10, bg="black", text="Union", fg="white", command=lambda:self.union()).grid(row=0, column=0, padx=10, pady=10)
        self.__btn_diferencia = Button(self.__miframe, width=10, bg="black", text="Diferencia", fg="white", command=lambda:self.diferencia()).grid(row=1, column=0, padx=10, pady=10)
        self.__btn_interseccion = Button(self.__miframe, width=10, bg="black", text="Interseccion", fg="white", command=lambda:self.interseccion()).grid(row=2, column=0, padx=10, pady=10)
        self.__btn_salir= Button(self.__miframe, width=10, bg="black", text="Salir", fg="white", command=lambda:self.salir()).grid(row=3, column=0, padx=10, pady=10)

    
    def construir(self) -> None:
        "" "Metodo para construir la ventana" ""
        self.__raiz.mainloop()
    
    
    def agregarEnPantalla(self, conjunto: str) -> None:
        "" "Metodo para mostrar en pantalla la ventana" ""
        self.__lsb_texto.insert(self.__indice, conjunto)
        self.__indice+=1
        self.__lsb_texto.insert(self.__indice, "")
        self.__indice+=1
    
    
    def salir(self) -> None:
        "" "Metodo de evento para salir" ""
        self.__raiz.destroy()
    
    
    def union(self) -> None:
        "" "Metodo de evento para llamar a la union de la clase Conjunto" ""
        self.agregarEnPantalla(f"Union:    {self.__conjunto.union()}")


    def interseccion(self) ->None:
        "" "Metodo de evento para llamar a la interseccion de la clase Conjunto" ""
        self.agregarEnPantalla(f"Interseccion:    {self.__conjunto.interseccion()}")
        

    def diferencia(self) -> None:
        "" "Metodo de evento para llamar a la diferencia de la clase Conjunto" ""
        self.agregarEnPantalla(f"Diferencia:    ConjuntoDivisor - CojuntoPrimo:    {self.__conjunto.diferencia()[0]}")
        self.__indice-=1
        self.agregarEnPantalla(f"Diferencia:    ConjuntoPrimo - CojuntoDivisor:    {self.__conjunto.diferencia()[1]}")
