'''
Created on 24 sep. 2020

@author: RSSpe
'''

TITULO = "Conjuntos"

ALTO = 30
ANCHO = 115
TEXTO = f'''Selecciona el fondo blanco y usa las flecha del teclado cuando
        se llene la pantalla para ver lo que este por debajo del listado'''


def generarConjuntoDivisores() -> set:
    "" "Funcion para crear un conjunto de numeros primos" ""
    conjunto = []

    for i in range(1, 351):
            if 350%i==0:
                conjunto.append(i)

    return set(conjunto)

def generarConjuntoPrimo() -> set:
    "" "Funcion para crear un conjunto de numeros primos" ""
    conjunto = []
        
    for i in range(101):
        cont=0
        for j in range(1, i+1):
            if i%j==0:
                cont+=1

        if cont==2:
            conjunto.append(i)
    
    return set(conjunto)
